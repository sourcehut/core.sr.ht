from flask import Blueprint, current_app, send_from_directory
from os import getcwd, path
from srht.config import cfg

static_blueprint = Blueprint('srht_static', __name__)

assets = cfg("sr.ht", "assets", default="/usr/share/sourcehut")

@static_blueprint.route("/static/<path:filename>")
def static_asset(filename):
    filename = path.normpath(filename)

    if current_app.debug:
        dev_path = path.join(getcwd(), 'static', current_app.site)
        if path.exists(path.join(dev_path, filename)):
            return send_from_directory(dev_path, filename)

    site_assets = path.join(assets, 'static', current_app.site)
    if path.exists(path.join(site_assets, filename)):
        return send_from_directory(site_assets, filename)

    shared_assets = path.join(assets, 'static')
    return send_from_directory(shared_assets, filename)
