from enum import Enum
from typing import Union

class Error(Enum):
    """
    Standard error codes defined by core-go.
    """
    ACCESS_DENIED = "ERR_ACCESS_DENIED"
    NOT_FOUND = "ERR_NOT_FOUND"

class GraphQLError(Exception):
    def __init__(self, body):
        self.body = body
        self.errors = body["errors"]
        self.data = body.get("data")

    def has(self, code: Union[str, Error], path: list[str]=None):
        if isinstance(code, Error):
            code = code.value
        """
        Returns true if the given standard error code is present.

        If path is not None, only tests the specified path.
        """
        for err in self.errors:
            present = err.get("code") == code
            if not present:
                continue
            if path is None:
                return True
            if err["path"] == path:
                return True
        return False
